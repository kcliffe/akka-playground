using System;
using Akka.Actor;

namespace AkkaSamples
{

	public class ChildActor : ReceiveActor
	{
        // private IActorRef grandChildRef;

        public ChildActor()
		{
            Console.WriteLine("Hello from {0}", Self);

			Receive<SendMsg>(msg => Sender.Tell(
				new ReplyMsg(string.Format("replying to {0} with ... x+1", msg.Msg))));

			Receive<EchoMsg>(msg => { 
				var props = Props.Create<GrandChildActor>();
				IActorRef grandChildRef = Context.ActorOf(props, "grandChildRef");

				grandChildRef.Tell(msg);
			});
		}
	}
    
}
