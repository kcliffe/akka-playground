using System;
using Akka.Actor;

namespace AkkaSamples
{
    public class GrandChildActor : ReceiveActor
    {
        IDatabase theBigScaryDb;
		int retries = 0;

        public GrandChildActor()
        {
            Console.WriteLine("Hello from {0}", Self);
            theBigScaryDb = new BigScaryDb();

            Receive<EchoMsg>(msg =>
            {
                if (msg.Msg.Contains("666"))
                {
                    throw new Exception("I did a bad thing");
                }

                theBigScaryDb.Write(msg.Msg);
            });
        }

		public override void AroundPreRestart (Exception cause, object message)
		{
			Console.WriteLine ("I'm a grandchild. I died because somebody made me say {0}. But i'm restarting...", 
				((EchoMsg)message).Msg);

			base.AroundPreRestart (cause, message);
		}
    }
}
