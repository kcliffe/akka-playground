﻿using System;
using Akka.Actor;

namespace AkkaSamples
{
	public class ParentActor : ReceiveActor
	{
	    private IActorRef childRef;

        public ParentActor()
		{
            Console.WriteLine("Hello from {0}", Self);

			var props = Props.Create<ChildActor>();
			childRef = Context.ActorOf(props, "childRef");

			Receive<EchoMsg>(msg => childRef.Tell(msg));
		}
	}
		
    internal interface IDatabase
    {
        void Write(string stuff);
    }

    internal class BigScaryDb : IDatabase
    {
        public void Write(string stuff)
        {
            Console.WriteLine(stuff);
        }
    }
}

