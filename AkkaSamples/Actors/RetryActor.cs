﻿using System;
using Akka.Actor;

namespace AkkaSamples
{
	public class RetryActor : UntypedActor
	{
		IActorRef riskyChildRef;

		public RetryActor ()
		{
			var props = Props.Create<R2>();
			riskyChildRef = Context.ActorOf(props, "childRef");
		}

		protected override void OnReceive(object message)
		{
			Console.WriteLine (message);

			// pass the message to the child - this would typically result 
			// in some risky activity - db access / file / network io etcu
			riskyChildRef.Forward (message);
		}

		/// <summary>
		/// Supervisor strategy when child actor throws an exception
		/// </summary>
		/// <returns></returns>
		protected override SupervisorStrategy SupervisorStrategy()
		{
			return new OneForOneStrategy( //or AllForOneStrategy
				3, // num retries
				TimeSpan.FromSeconds(1000), // duration within?
				x =>
				{
					if (x is RetryableException)
					{
						((RetryableException)x).RetryCount++;
						Console.WriteLine("Exception: " + x);
						return Directive.Restart;
					}

					return Directive.Escalate;
				});
		}
	}

	public class R2 : UntypedActor
	{
		int retryCount;

		public R2 ()
		{
			retryCount = 0;	
		}

		protected override void OnReceive (object message)
		{
			if (message.ToString () == "error") {
				throw new RetryableException ("should retry - until it doesn't") {
					RetryCount = retryCount
				};
			}
		}

		protected override void PreRestart (Exception reason, object message)
		{
			// attempt to process the message again
			if (message != null)
				Self.Forward (message);
			
			base.PreRestart (reason, message);
		}

		protected override void PostRestart (Exception reason)
		{
			var ex = reason as RetryableException;
			if (ex != null) {
				Console.WriteLine ("Restarting R2 ({0} retries)", ex.RetryCount);

				// set state so it can be flowed back into the exception for demo purposes..
				retryCount = ex.RetryCount;
			}

			base.PostRestart (reason);
		}
	}

	internal class RetryableException : Exception
	{
		public int RetryCount;

		public RetryableException (string message) : base(message) {}
	}
}

