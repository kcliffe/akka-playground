using System;
using Akka.Actor;

namespace AkkaSamples
{
	public class SampleActor : ReceiveActor
	{
		// state 
		private int counter = 0;

		public SampleActor ()
		{
			Receive<IncrementMsg>(msg => {
				counter++;

				Console.WriteLine("actor {0} counter now = {1}", Self.ToString(),  counter);
			});

			Receive<SendMsg> (msg => {

				Console.WriteLine("Msg {0} recieved, passing to somebody else..", msg.Msg);

				var props = Props.Create<ChildActor>();
				var childRef = Context.ActorOf(props, "childRef");

				childRef.Tell(msg);
			});

			Receive<ReplyMsg> (msg => Console.WriteLine (msg.Msg));

			// note, EchoMsg is the base of polymorphic msgs SengMsg and ReplyMsg
			// if we don't put it down here, it consumes them!
			Receive<EchoMsg>(msg => Console.WriteLine("actor {0} says {1}", Self.ToString(),  msg.Msg));
		}

		// lifecycles below

		protected override void PostStop()
		{
			Console.WriteLine("actor {0} then stops (dead)", Self);
			base.PostStop ();
		}

		protected override void PostRestart (Exception reason)
		{
			Console.WriteLine("actor {0} is in PostRestart", Self);
			base.PostRestart (reason);
		}

		protected override void PreRestart (Exception reason, object message)
		{
			Console.WriteLine("actor {0} is in PreRestart", Self);
			base.PreRestart (reason, message);
		}

		public override void AroundPreStart ()
		{
			Console.WriteLine("actor {0} is in a-PreRestart", Self);
			base.AroundPreStart ();
		}
	}
}
