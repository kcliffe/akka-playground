﻿using System;

namespace AkkaSamples
{
	public class EchoMsg
	{
		public string Msg { get; private set; }

		public EchoMsg (string echo)
		{
			this.Msg = echo;
		}
	}
}

