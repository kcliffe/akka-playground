using System;
using Akka.Actor;

namespace AkkaSamples
{
	public class ReplyMsg : EchoMsg
	{
		public ReplyMsg (string msg) : base(msg)
		{
		}
	}
}
