﻿using System;
using Akka.Actor;

namespace AkkaSamples
{
	class MainClass
	{
		public static ActorSystem ActorSystem;

		public static void Main (string[] args)
		{
			// initialize MyActorSystem
			ActorSystem = ActorSystem.Create("MyActorSystem");

			//new ActorsHaveState ().Run (ActorSystem);
			//new ActorsDontLikePoison().Run(ActorSystem);
		    //new ActorsCanCreateHierarchies().Run(ActorSystem);
            //new ActorsCanCreateHierarchies_And_They_Can_Supervise_Eachother().Run(ActorSystem);
			//new ActorsKnowWhoAsked().Run(ActorSystem);
			//new ActorsWillProcessCurrentMessageBeforeTheyDie().Run(ActorSystem);
			//new ActorsCanSpecifyTheirSupervisorStrategy().Run(ActorSystem);

			// blocks the main thread from exiting until the actor system is shut down
			ActorSystem.AwaitTermination();
		}
	}

	class Actors_Have_State
	{
		public void Run(ActorSystem actorSystem)
		{
			Props props = Props.Create<SampleActor>();
			actorSystem.ActorOf(props, "sampleActor");
		}
	}

	class Actors_Like_To_Talk
	{
		public void Run(ActorSystem actorSystem)
		{
			Props props = Props.Create<SampleActor>();
			IActorRef sampleActor = actorSystem.ActorOf(props, "sampleActor");
			IActorRef sampleActor2 = actorSystem.ActorOf(props, "sampleActor2");

			// Tell is async. 
			sampleActor.Tell (new IncrementMsg ());
			sampleActor2.Tell (new IncrementMsg ());
			sampleActor.Tell (new IncrementMsg ());
		}
	}

	class Actors_Can_Return_To_Sender
	{
		public void Run(ActorSystem actorSystem)
		{
			Props props = Props.Create<SampleActor>();
			IActorRef sampleActor = actorSystem.ActorOf(props, "sampleActor");

			sampleActor.Tell (new SendMsg ("x ... "));
		}
	}

	class Actors_Dont_Like_Poison
	{
		public void Run(ActorSystem actorSystem)
		{
			var props = Props.Create<SampleActor>();
			IActorRef sampleActor = actorSystem.ActorOf(props, "sampleActor");

			sampleActor.Tell (new EchoMsg ("gidday"));
			sampleActor.Tell (PoisonPill.Instance);
		}
	}

	class Actors_Will_Process_Current_Message_BeforeTheyDie
	{
		public void Run(ActorSystem actorSystem)
		{
			var props = Props.Create<HariKariActor>();
			IActorRef sampleActor = actorSystem.ActorOf(props, "hkActor");

			sampleActor.Tell ("Sleep!");
			sampleActor.Tell (Akka.Actor.Kill.Instance);
		}
	}

	class Actors_Can_CreateHierarchies
	{
		public void Run(ActorSystem actorSystem)
		{
			Props props = Props.Create<ParentActor> ();
			actorSystem.ActorOf (props, "parentActor");
		}
	}

    class Actors_Supervise_Eachother_In_Hierarchies
    {
        public void Run(ActorSystem actorSystem)
        {
            // nothing here where our actors used to be!
            var props = Props.Create<ParentActor>();
            IActorRef parentActor = actorSystem.ActorOf(props, "parentActor");

            parentActor.Tell(new EchoMsg("Yay!"));
            parentActor.Tell(new EchoMsg("666!"));
        }
    }
		
	class ActorsHaveAddressesAndYouCanLookupThemUp
	{
	}

	class ActorsCanWatch 
	{
	}

	class ActorsHaveALifecycle
	{
		// todo - Take SampleActor, remove lifecycle, create LifeCycleActor
	}

	class ActorsHaveAMailbox {
	}

	class FailureToDeliverAMessageMeansItGoesToTheDeadLetterBox
	{
	}

	class ActorsCanSpecifyTheirSupervisorStrategy
	{
		public void Run(ActorSystem actorSystem)
		{
		// nothing here where our actors used to be!
			var props = Props.Create<RetryActor> ();
			IActorRef retryActor = actorSystem.ActorOf (props, "retryActor");

			retryActor.Tell ("1");
			retryActor.Tell ("2");
			retryActor.Tell ("3");

			retryActor.Tell ("error");
		}
	}
}
