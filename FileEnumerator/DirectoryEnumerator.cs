using System;
using Akka.Actor;
using System.IO;
using System.Linq;

namespace FileEnumerator
{
	public class DirectoryEnumerator : UntypedActor
	{
		protected override void OnReceive (object message)
		{
			var filesInDirectory = Directory.EnumerateFiles((string)message).ToList();
			Console.WriteLine ("Discovered dir {0} with {1} files", message, filesInDirectory.Count);

			foreach (var file in filesInDirectory) {
				Props props = Props.Create<DirectoryEnumerator> ();//.WithSupervisorStrategy(
				//	new OneForOneStrategy(3,TimeSpan.FromSeconds(1), );

				IActorRef dirEnum = actorSystem.ActorOf(props, "directoryEnum");
			}
		}
	}

}
