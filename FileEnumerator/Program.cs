﻿using System;
using Akka.Actor;
using System.IO;

namespace FileEnumerator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Enter a path root (may be relative):");
			var root = Console.ReadLine ();

			var actorSystem = ActorSystem.Create("MyActorSystem");

			if (ValidatePath(root))
			{
				Props props = Props.Create<DirectoryEnumerator> ();//.WithSupervisorStrategy(
				//	new OneForOneStrategy(3,TimeSpan.FromSeconds(1), );

				IActorRef dirEnum = actorSystem.ActorOf(props, "directoryEnum");
				dirEnum.Tell (root);
			}

			actorSystem.AwaitTermination ();
		}

		public static bool ValidatePath(string path)
		{
			if (Path.IsPathRooted(path))
			{
				return Directory.Exists (path);
			}

			return false;
		}
	}

					// public static 
}
