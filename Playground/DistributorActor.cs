using System;
using Akka.Actor;
using System.Collections.Generic;
using Akka.Routing;

namespace Playground
{
	/// <summary>
	/// A distributer is reponsible for flowing messages to a router
	/// based on information encoded into a the task (message).
	/// 
	/// If the router does not exist, we create it. By default we 
	/// currently create and attach 5 workers.
	/// </summary>
	public class DistributorActor : UntypedActor
	{
		// IActorRef lookups are probably quite cheap
		// but not so mch when they might be remote?
		private Dictionary<string, IActorRef> actorCache = new Dictionary<string, IActorRef>();

		protected override void OnReceive (object message)
		{
			var router = TryGetRouter ((string)message);
			router.Forward (message);
		}

		private IActorRef TryGetRouter(string message)
		{
			string routerPath = string.Format ("/user/router_{0}", message);
			if (!actorCache.ContainsKey (routerPath))
			{
				actorCache.Add(routerPath, CreateRouter (Context, message));
			}

			return actorCache [routerPath];
		}

		public static IActorRef CreateRouter(IUntypedActorContext context, string message)
		{
			List<IActorRef> workers = new List<IActorRef> ();
			for(int i=0; i<5; i++)
			{
				var props = Props.Create<WorkerActor>();
				workers.Add(Context.ActorOf(props, string.Format("Worker{0}{1}",i,message)));
			}

			return context.ActorOf(Props.Empty.WithRouter(new RoundRobinGroup(workers.ToArray())));
		}
	}

}
