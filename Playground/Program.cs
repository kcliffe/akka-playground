﻿using System;
using Akka.Actor;

namespace Playground
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			ActorSystem actorSystem = ActorSystem.Create ("It");

			var props = Props.Create<DistributorActor>();
			var distributor = actorSystem.ActorOf(props, "Distributor");

			for (int i = 0; i < 5; i++) {
				distributor.Tell ("routeA");
				distributor.Tell ("routeB");
				distributor.Tell ("routeC");
				distributor.Tell ("routeA");
			}

			actorSystem.AwaitTermination ();
		}
	}
}
