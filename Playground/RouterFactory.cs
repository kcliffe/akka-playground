using System;
using Akka.Actor;
using System.Collections.Generic;
using Akka.Routing;

namespace Playground
{
	public class RouterFactory
	{
		public static IActorRef CreateRouter(IUntypedActorContext context, string path)
		{
			var router = context.ActorOf(Props.Empty.WithRouter(new RoundRobinGroup(
				"user/Worker1", "user/Worker2", "user/Worker3", "user/Worker4")));
		}
	}
}
