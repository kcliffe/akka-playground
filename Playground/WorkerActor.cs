using System;
using Akka.Actor;
using System.Collections.Generic;
using Akka.Routing;

namespace Playground
{
	public class WorkerActor : UntypedActor
	{
		protected override void OnReceive (object message)
		{
			Console.WriteLine ("Actor {0} received", this.Self.Path);
		}
	}
}
